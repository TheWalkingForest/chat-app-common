use actix_web::{HttpResponse, ResponseError};
use strum::Display;

#[derive(Display, Debug)]
pub enum UserError {
    NotFound,
    NotCreated,
}

impl ResponseError for UserError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            UserError::NotFound => HttpResponse::NotFound().finish(),
            _ => HttpResponse::InternalServerError().finish(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::errors::user::UserError;

    #[test]
    fn display() {
        assert_eq!(format!("{}", UserError::NotFound), "NotFound");
        assert_eq!(format!("{}", UserError::NotCreated), "NotCreated");
    }
}
