use actix_web::{HttpResponse, ResponseError};
use strum::Display;

#[derive(Display, Debug)]
pub enum RepoError {
    PutError,
    GetError,
    InitError,
    UpdateError,
}

impl ResponseError for RepoError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            RepoError::PutError => HttpResponse::InternalServerError().finish(),
            RepoError::GetError => HttpResponse::InternalServerError().finish(),
            RepoError::InitError => HttpResponse::InsufficientStorage().finish(),
            RepoError::UpdateError => HttpResponse::InternalServerError().finish()
        }
    }
}

#[cfg(test)]
mod test {
    use crate::errors::repository::RepoError;

    #[test]
    fn display() {
        assert_eq!(format!("{}", RepoError::GetError), "GetError");
        assert_eq!(format!("{}", RepoError::PutError), "PutError");
        assert_eq!(format!("{}", RepoError::InitError), "InitError");
        assert_eq!(format!("{}", RepoError::UpdateError), "UpdateError");
    }
}