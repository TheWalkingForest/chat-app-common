use actix_web::{HttpResponse, ResponseError};
use deadpool_postgres::PoolError;
use derive_more::{Display, From};
use tokio_pg_mapper::Error as TPGMError;
use tokio_postgres::error::Error as TPGError;

#[derive(Display, From, Debug)]
pub enum PGError {
    NotFound,
    PGError(TPGError),
    PGMError(TPGMError),
    PoolError(PoolError),
}
impl std::error::Error for PGError {}

impl ResponseError for PGError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            PGError::NotFound => HttpResponse::NotFound().finish(),
            PGError::PoolError(ref err) => {
                HttpResponse::InternalServerError().body(err.to_string())
            }
            _ => HttpResponse::InternalServerError().finish(),
        }
    }
}
