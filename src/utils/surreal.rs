use surrealdb::sql::{Object, Value};
use surrealdb::Response;
use anyhow::{anyhow, Result};

pub fn into_iter_object(ress: Vec<Response>) -> Result<impl Iterator<Item = Result<Object>>> {
    // let res = ress.into_iter().next().map(|rp| rp.result).transpose()?;
    let res = ress.into_iter().next().unwrap().transpose()?;

    match res {
        Some(Value::Array(arr)) => {
            let it = arr.into_iter().map(|v| match v {
                Value::Object(object) => Ok(object),
                _ => Err(anyhow!("A record was not an object")),
            });
            Ok(it)
        },
        _ => Err(anyhow!("No records found")),
    }
}

pub fn into_object_array(ress: Vec<Response>) -> Result<surrealdb::sql::Array> {
    let res = ress.into_iter().next().map(|rp| rp.result).transpose()?;
    match res {
        Some(Value::Array(arr)) => Ok(arr),
        _ => Err(anyhow!("No records found")),
    }
}
