use std::collections::BTreeMap;

#[allow(unused)]
fn map_to_json(map: &BTreeMap<String, String>) -> String {

    let mut result = "{".to_string();

    let mut iter = map.iter();
    let size = iter.size_hint();
    let mut i = 0;
    for pair in iter {
        let (key, val) = pair;
        if i < size.0 - 1 {
            result = format!("{}\"{}\": \"{}\",", result, *key, *val);
        }
        else {
            result = format!("{}\"{}\": \"{}\"}}", result, *key, *val);
        }
        i += 1;
    }

    result
}

#[cfg(test)]
mod serde {
    use std::collections::BTreeMap;
    use crate::utils::serde::map_to_json;

    #[test]
    fn btree_to_json() {
        let map: BTreeMap<String, String> = [
            ("username".to_string(), "test_user".to_string()),
        ].into();

        let map2: BTreeMap<String, String> = [
            ("uid".to_string(), "1234".to_string()),
            ("username".to_string(), "test_user".to_string()),
        ].into();

        assert_eq!(map_to_json(&map), "{\"username\": \"test_user\"}".to_string());
        assert_eq!(map_to_json(&map2), "{\"uid\": \"1234\",\"username\": \"test_user\"}".to_string());
    }
}